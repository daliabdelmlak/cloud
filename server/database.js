const mysql = require('mysql2/promise');

const pool = mysql.createPool({
    "host": "192.168.96.135",
    "user": "root",
    "password": "root_password",
    "database": "cloud"
  });

module.exports = pool;
