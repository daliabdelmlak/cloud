const express = require('express');
const bodyParser = require('body-parser');
const pool = require('./database'); // Import MySQL pool from the config file
const cors = require('cors'); // Import CORS middleware

const app = express();
const port = 3000;
app.use(cors());

// Middleware to parse JSON bodies
app.use(bodyParser.json());

// Endpoint to create a new post
app.post('/posts', async (req, res) => {
  const { title, content } = req.body;
  try {
    const [result] = await pool.query('INSERT INTO posts (title, content) VALUES (?, ?)', [title, content]);
    res.status(201).send('Post created successfully');
  } catch (error) {
    console.error('Error creating post:', error);
    res.status(500).send('Error creating post');
  }
});

// Endpoint to get the list of posts
app.get('/posts', async (req, res) => {
  try {
    const [results] = await pool.query('SELECT * FROM posts');
    res.json(results);
  } catch (error) {
    console.error('Error getting posts:', error);
    res.status(500).send('Error getting posts');
  }
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

